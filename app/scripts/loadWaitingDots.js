import helpers from './helpers/helpers';

function loadWaitingDots(type) {
  const htmlEl = helpers.createHtmlEl('div', `${type}-message ${type}-message--waiting animated fadeIn fadeOut`);
  let html = `
    <div class="dots--${type}">
    </div>
    <div class="dots--${type}">
    </div>
    <div class="dots--${type}">
    </div>
  `;
  if (type === 'recived') {
    html += helpers.profileImgHtml();
  } else {
    html += helpers.userImgHtml();
  }
  htmlEl.innerHTML = html;
  return htmlEl;
}

export default loadWaitingDots;
