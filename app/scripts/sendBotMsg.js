import loadWaitingDots from './loadWaitingDots';
import helpers from './helpers/helpers';
import analyzeUserInput from './analyzeUserInput';
import displayDotsThenMsg from './displayDotsThenMsg';

function sendBotMsg(userInput) {
  helpers.appendText(loadWaitingDots('recived'));
  helpers.scrollToLastMsg();
  console.log(analyzeUserInput(userInput));
  displayDotsThenMsg(1000, { message: true, content: userInput });
}

export default sendBotMsg;
