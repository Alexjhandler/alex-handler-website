module.exports = {
  createHtmlEl(type, className) {
    const el = document.createElement(type);
    el.setAttribute('class', className);
    return el;
  },
  profileImgHtml(extraClass) {
    const htmlEl = `<img class='message__img ${extraClass}' src='images/profile.jpg'>`;
    return htmlEl;
  },
  userImgHtml() {
    const htmlEl = '<div class=\'message__img message__img--sent\'><i class=\'icon-user\'></i></div>';
    return htmlEl;
  },
  startingStrings: {
    hello: '<p class=\'recived-message__text\'>Hi! I\'m Alex. I\'m a Front-end engineer based out of Austin, TX. You can talk to me here if you like.</p><img class=\'message__img\' src=\'images/profile.jpg\'>',
    askMe: '<p class=\'recived-message__text\'>You can ask me to show you some of my work, or to get in touch or just learn a bit about me.<img class=\'message__img\' src=\'images/profile.jpg\'>',
    skip: '<p class=\'recived-message__text\'>Or, if you\'re not much of the talking type you can look at some of my work <a href=\'\' class=\'message-link\'>here</a> And get in touch <a href=\'\' class=\'message-link\'>here</a></p><img class=\'message__img\' src=\'images/profile.jpg\'>'
  },
  createSentText(userText) {
    const sentMessage = this.createHtmlEl('div', 'js-msg sent-message animated fadeInRight');
    const html = `<p class='sent-message__text'>${userText}</p>${this.userImgHtml('message__img--sent message__img--sent--full')}`;
    sentMessage.innerHTML = html;
    return sentMessage;
  },
  createReciveText(userText) {
    const recivedMessage = this.createHtmlEl('div', 'js-msg recived-message recived-message--animated');
    const html = `<p class='recived-message__text'>${userText}</p>${this.profileImgHtml()}`;
    recivedMessage.innerHTML = html;
    return recivedMessage;
  },
  replaceConvoBit(toReplace, htmlContent) {
    const replaceEl = document.querySelector(toReplace);
    const newEl = this.createReciveText(htmlContent);
    replaceEl.replaceWith(newEl);
  },
  scrollToLastMsg() {
    const latestMsg = document.querySelector('.messages').lastElementChild;
    latestMsg.scrollIntoView(true, { behavior: 'smooth' });
  },
  appendText(el) {
    const messages = document.querySelector('.messages');
    messages.appendChild(el);
  }
};
