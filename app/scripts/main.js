// import Floatl from 'floatl'

import loadWaitingDots from './loadWaitingDots';
import loadStartingConvo from './loadStartingConvo';
import helpers from './helpers/helpers';
import sendUserMsg from './sendUserMsg';
import sendBotMsg from './sendBotMsg';


const userForm = document.querySelector('.messanger');
const messageForm = document.getElementById('message');
//
// const floatls = document.querySelectorAll('.floatl');
// console.log(floatls)

document.addEventListener('DOMContentLoaded', () => {
  loadStartingConvo();
});

messageForm.addEventListener('submit', (e) => {
  e.preventDefault();
  const userText = document.querySelector('.messanger').value;
  sendUserMsg(userText);
  sendBotMsg(userText);
});

userForm.addEventListener('focus', () => {
  helpers.appendText(loadWaitingDots('sent'));
  helpers.scrollToLastMsg();
});

userForm.addEventListener('focusout', () => {
  const dots = document.querySelector('.sent-message--waiting');
  dots.remove();
});
