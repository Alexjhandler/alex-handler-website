import helpers from './helpers/helpers';
import loadWaitingDots from './loadWaitingDots';


function disableOrEnableForms(bool) {
  const userSubmit = document.querySelector('.messanger-submit');
  const userForm = document.querySelector('.messanger');
  userSubmit.disabled = bool;
  userForm.disabled = bool;
}

function displayDotsThenMsg(time, ops) {
  setTimeout(() => {
    if (ops.message) {
      helpers.replaceConvoBit('.recived-message--waiting', ops.content);
    } else {
      helpers.appendText(loadWaitingDots('recived'));
    }
    if (ops.firstMsg) { disableOrEnableForms(true); }
    if (ops.lastMsg) { disableOrEnableForms(false); }
    helpers.scrollToLastMsg();
  }, time);
}

export default displayDotsThenMsg;
