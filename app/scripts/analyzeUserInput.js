import nlp from 'compromise';

function analyzeUserInput(input) {
  const doc = nlp(input);
  // debugger
  return doc.terms().data();
}
export default analyzeUserInput;
