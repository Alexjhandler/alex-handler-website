import helpers from './helpers/helpers';

function clearForm(el) {
  const element = el;
  element.value = '';
}

function sendUserMsg(userInput) {
  const userForm = document.querySelector('.messanger');
  helpers.appendText(helpers.createSentText(userInput));
  userForm.blur();
  clearForm(userForm);
  helpers.scrollToLastMsg();
}

export default sendUserMsg;
