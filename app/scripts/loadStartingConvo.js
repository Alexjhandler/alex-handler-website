import loadWaitingDots from './loadWaitingDots';
import helpers from './helpers/helpers';
import displayDotsThenMsg from './displayDotsThenMsg';

function loadStartingConvo() {
  helpers.appendText(loadWaitingDots('recived'));
  displayDotsThenMsg(
    1000,
    {
      message: true,
      content: helpers.startingStrings.hello,
      firstMsg: true
    }
  );
  displayDotsThenMsg(2000, { message: false });
  displayDotsThenMsg(
    3000,
    {
      message: true,
      content: helpers.startingStrings.askMe
    }
  );
  displayDotsThenMsg(4000, { message: false });
  displayDotsThenMsg(
    5000,
    {
      message: true,
      content: helpers.startingStrings.skip,
      lastMsg: true
    }
  );
}

export default loadStartingConvo;
